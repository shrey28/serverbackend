const jwt = require("jsonwebtoken");

const User = require("../model/userSchema");


const SECRET_KEY = "SHREYPATHAKASHWARYPATHAK99773344289977334428";

const Authentication = async (req, res, next) => {
  try {
    const token=req.cookies.jwtoken;
    console.log(token)
    const verifyToken=jwt.verify(token,SECRET_KEY);
    console.log(verifyToken);
    const rootUser=await User.findOne({_id:verifyToken._id,"tokens.token":token})
    if(!rootUser){throw new Error("user not found")}
    req.token=token;
    req.rootUser=rootUser;
    req.userID=rootUser._id;
    next();
  } catch (err) {
    res.status(401).send("no token found");
        console.log(err);
  }
};

module.exports = Authentication;
