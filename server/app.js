const mongoose = require("mongoose");
const express = require("express");
const app = express();
const cookieParser = require('cookie-parser');

app.use(cookieParser());

require("./db/conn");

app.use(express.json());
//const User = require('./model/userSchema');
app.use(require("./Router/auth"));

const PORT = 5000;


// const middleware = (req, res, next) => {
//   console.log(`Hello My middleware`);
//   next();
// };

// app.get("/", (req, res) => {
//   res.send(`Hellow World from the server`);
// });

// app.get("/about", middleware, (req, res) => {
//   res.send(`Hellow World from the server about`);
// });

// app.get("/contact", (req, res) => {
//   res.send(`Hellow World from the server contact`);
// });
// app.get("/singin", (req, res) => {
//   res.send(`Hellow World from the server singin`);
// });
// app.get("/singup", (req, res) => {
//   res.send(`Hellow World from the server singup`);
// });

app.listen(PORT, () => {
  console.log(`server is runnig ${PORT}`);
});
