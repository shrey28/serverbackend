const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const Authentication = require("../middleware/authenticate");

require("../db/conn");
const User = require("../model/userSchema");

router.get("/", async (req, res) => {
  res.send(`Hellow World from the server Router`);
});

//Promise Code

// router.post("/Registertion", (req, res) => {
//   const { name, email, phone, work, password, cpassword } = req.body;

//   if (!name || !email || !phone || !work || !password || !cpassword) {
//     return res.status(442).json({ error: "Plz fill field" });
//   }

//   User.findOne({ email: email })
//     .then((userExist) => {
//       if (userExist) {
//         return res.status(442).json({ error: "Email already Exist" });
//       }

//       const user = new User({ name, email, phone, work, password, cpassword });

//       user
//         .save()
//         .then(() => {
//           res.status(201).json({ message: "user reqisterd Successfuly" });
//         }).catch((err)=>{
//             res.status(500).json({error: "Failed to registrtion"});
//         })

//     })
//     .catch(err => {
//       console.log(err);
//     });
// });

// async await code
router.post("/register", async (req, res) => {
  const { name, email, phone, work, password, cpassword } = req.body;

  if (!name || !email || !phone || !work || !password || !cpassword) {
    return res.status(442).json({ error: "Plz fill field" });
  }

  try {
    const userExist = await User.findOne({ email: email });
    if (userExist) {
      return res.status(442).json({ error: "Email already Exist" });
    } else if (password != cpassword) {
      return res.status(442).json({ error: "Email already Exist" });
    } else {
      const user = new User({ name, email, phone, work, password, cpassword });

      await user.save();

      res.status(201).json({ message: "user reqisterd Successfuly" });
    }
  } catch (err) {
    console.log(err);
  }
});

router.post("/signin", async (req, res) => {
  let token;

  try {
    const { email, password } = req.body;

    if (!email || !password) {
      return res.status(501).json({ error: "Please Filled the data" });
    }

    const userLogin = await User.findOne({ email: email });

    // console.log(userLogin);
    if (userLogin) {
      const isMatch = await bcrypt.compare(password, userLogin.password);

      const token = await userLogin.generateAuthToken();

      console.log(token);

      res.cookie("jwtoken", token, {
        expires: new Date(Date.now() + 25892000000),
        httpOnly: true,
      });

      if (!isMatch) {
        res.status(400).json({ error: "Invalid Creadientials PassWord" });
      } else {
        res.json({ message: "User Sigin SuccessFully" });
      }
    } else {
      res.status(400).json({ error: "Invalid Creadientials" });
    }
  } catch (err) {
    console.log(err);
  }
});

//about us page

router.get("/about", Authentication, (req, res) => {
  res.send(req.rootUser);
});

//get uesedata contact us and home
router.get("/gatedata", Authentication, (req, res) => {
  res.send(req.rootUser);
});

//contact page
router.post("/contact", Authentication, async (req, res) => {
  try {
    const { name, email, phone, message } = req.body;
    
    if (!name || !email || !phone || !message ) {
      console.log("error contact form")
      return res.json({ error: "please filled to contact form" });
    }
    const userContact = await User.findOne({ _id: req.userID });
    if (userContact) {
      const userMess = await userContact.addMessage(
        name,
        email,
        phone,
        message
      );
      
      await userContact.save();
      
      res.status(201).json({ message: "user contact seccessfully" });
    }
  } catch (error) {
    console.log(error);
  }
}); 

module.exports = router;
