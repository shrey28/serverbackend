import React, { useState } from "react";
import "../cssfiles/Login.css";
import { NavLink } from "react-router-dom";
import { useNavigate } from "react-router-dom";

const Signup = () => {
  const datas = {
    name: "",
    email: "",
    phone: "",
    work: "",
    password: "",
    cpassword: "",
  };
  const [user, setUser] = useState(datas);
  const history = useNavigate();

  let names, value;
  const handalinput = (e) => {
    names = e.target.name;
    value = e.target.value;
    setUser({ ...user, [names]: value });
    // console.log(e);
  };

  const PostData = async(e)=>{
    e.preventDefault();
    const {name,email, phone,work,password,cpassword}=user;

   const res = await fetch('/register',{
    method:"POST",
    headers:{
      "Content-Type":"application/json"
    },
    body:JSON.stringify({
      name,email,phone,work,password,cpassword
    })

   })
   const data = await res.json();
   if(res.status===422||!data){
    window.alert("Invalid")
    console.log("Invalid")
   }else{
    window.alert("Register Succsesfully")
    console.log("Register Succsesfully")

    history('/login')
   }

  }

  return (
    <>
      <div className="container login">
        <form method="Post">
          <div className="title">Registration</div>
          <div className="input-box">
            <input
              type="text"
              name="name"
              className="form-control"
              placeholder="Enter your name"
              autoComplete="off"
              onChange={handalinput}
              value={user.name}
            />
          </div>
          <div className="input-box">
            <input
              type="email"
              className="form-control"
              name="email"
              placeholder="Enter Your Email"
              autoComplete="off"
              onChange={handalinput}
              value={user.email}
            />
          </div>
          <div className="input-box">
            <input
              type="tel"
              name="phone"
              className="form-control"
              placeholder="Enter your number"
              autoComplete="off"
              onChange={handalinput}
              value={user.phone}
            />
          </div>
          <div className="input-box">
            <input
             type="text"
             name="work"
             className="form-control"
             placeholder="Enter your name"
             autoComplete="off"
             onChange={handalinput}
             value={user.work}
            />
          </div>
          <div className="input-box">
            <input
              type="password"
              name="password"
              className="form-control"
              placeholder="Enter your password"
              autoComplete="off"
              onChange={handalinput}
              value={user.password}
            />
          </div>
          <div className="input-box">
            <input
              type="password"
              name="cpassword"
              className="form-control"
              placeholder="Confirm your password"
              autoComplete="off"
              onChange={handalinput}
              value={user.cpassword}
            />
          </div>
          <div className="input-box button">
            <input type="submit" name="signup" id="signup" value="Continue" onClick={PostData} />
          </div>
        </form>
      </div>

      <div className="sigup-image">
        <figure>
          <img src="" alt="" />
        </figure>
        <NavLink to="/login">Your Already register</NavLink>
      </div>
    </>
  );
};

export default Signup;
