import React,{useState} from "react";
import "../cssfiles/Login.css";
import { NavLink,useNavigate } from "react-router-dom";
const Login = () => {

  const [email,setEmail]=useState('');
  const [password,setPass]=useState('');
  const history = useNavigate();


  const logindata = async(e)=>{
    e.preventDefault();
   

   const res = await fetch('/signin',{
    method:"POST",
    headers:{
      "Content-Type":"application/json"
    },
    body:JSON.stringify({
      email,password
    })

   });
   const data = res.json();
   if(res.status === 400 || !data){
    window.alert("invalid credentials")
   }
   else{
    window.alert("User Login Seccessfull")
    history('/')
   }
  }

  return (
    <div class="container login">
      <form method="POST">
        <div class="title">Login</div>
        <div class="input-box">
          <input type="email" name="email"class="form-control" onChange={(e)=>setEmail(e.target.value)} value={email} placeholder="Enter Your Email" required id="email" />
          
        </div>
        <div class="input-box">
          <input type="password" name="password" class="form-control"onChange={(e)=>setPass(e.target.value)} value={password} placeholder="Enter Your Password" id="password" required />
          
        </div>
        <div class="input-box button">
          <input type="submit" name="singin"  id="singin" value="Log In" onClick={logindata}/>
        </div>
      </form>
      <NavLink to="/signup" style={{textDecoration: "none"}}>
        <div class="option">Create a Account</div>
      </NavLink>
    </div>
  );
};

export default Login;
