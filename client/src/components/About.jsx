import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import remove from "../imges/remove-bg.png";
import "bootstrap/dist/css/bootstrap.min.css";
import "../cssfiles/Login.css";

const About = () => {
  
  const history = useNavigate();
  const [userData, setUserData] = useState("");
  const verifyPage = async () => {
    try {
      const res = await fetch("/about", {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        credentials: "include",
      });
      const data = await res.json();
      setUserData(data)
      console.log(data);

      if (!res.status === 200) {
        const err = new Error(res.error);
        throw err;
      }
    } catch (err) {
      console.log(err);
      history("/login");
    }
  };

  useEffect(() => {
    verifyPage();
  }, []);

  return (
    <div className="container">
      <form method="GET">
        <div className="row">
          <div className="col-md-3">
            <img class="profilepic" src={remove} alt="inna" />

            <p>Work Link</p>
            <ul>
              <li>
                <a
                  href="https://www.youtube.com/watch?v=kHEhhV3EyPU&list=PLwGdqUZWnOp3t3qT7pvAznwUDzKbhEcCc&index=27&ab_channel=ThapaTechnical"
                  target="MERN Project"
                >
                  Youtube{" "}
                </a>
              </li>
              <li>
                <a
                  href="https://www.youtube.com/watch?v=kHEhhV3EyPU&list=PLwGdqUZWnOp3t3qT7pvAznwUDzKbhEcCc&index=27&ab_channel=ThapaTechnical"
                  target="MERN Project"
                >
                  Instagram
                </a>
              </li>
              <li>
                <a
                  href="https://www.youtube.com/watch?v=kHEhhV3EyPU&list=PLwGdqUZWnOp3t3qT7pvAznwUDzKbhEcCc&index=27&ab_channel=ThapaTechnical"
                  target="MERN Project"
                >
                  7min Coder
                </a>
              </li>
              <li>
                <a
                  href="https://www.youtube.com/watch?v=kHEhhV3EyPU&list=PLwGdqUZWnOp3t3qT7pvAznwUDzKbhEcCc&index=27&ab_channel=ThapaTechnical"
                  target="MERN Project"
                >
                  {" "}
                  Web Site GitLab MERN
                </a>
              </li>
              <li>
                <a
                  href="https://www.youtube.com/watch?v=kHEhhV3EyPU&list=PLwGdqUZWnOp3t3qT7pvAznwUDzKbhEcCc&index=27&ab_channel=ThapaTechnical"
                  target="MERN Project"
                >
                  Web Developer
                </a>
              </li>
              <li>
                <a
                  href="https://www.youtube.com/watch?v=kHEhhV3EyPU&list=PLwGdqUZWnOp3t3qT7pvAznwUDzKbhEcCc&index=27&ab_channel=ThapaTechnical"
                  target="MERN Project"
                >
                  Figma
                </a>
              </li>
              <li>
                <a
                  href="https://www.youtube.com/watch?v=kHEhhV3EyPU&list=PLwGdqUZWnOp3t3qT7pvAznwUDzKbhEcCc&index=27&ab_channel=ThapaTechnical"
                  target="MERN Project"
                >
                  Software Engineer
                </a>
              </li>
            </ul>
          </div>
          <div className="col-md-7">
        
            <h5>{userData.name}</h5>
            <h6>{userData.work}</h6>
            <p>
              RANGING : <span>1/10</span>
            </p>
            <ul
              class="nav nav-tabs"
              id="myTab"
              role="tablist"
              style={{ marginBottom: "15px" }}
            >
              <li class="nav-item" role="presentation">
                <button
                  class="nav-link active"
                 
                  data-bs-toggle="tab"
                  data-bs-target="#home"
                  type="button"
                  role="tab"
                  aria-controls="home"
                  aria-selected="true"
                >
                   About
                  
                </button>
              </li>
              <li class="nav-item" role="presentation">
                <button
                  class="nav-link"
                  id="profile-tab"
                  data-bs-toggle="tab"
                  data-bs-target="#profile"
                  type="button"
                  role="tab"
                  aria-controls="profile"
                  aria-selected="false"
                >
                 TimeLine
                </button>
              </li>
            </ul>





            <div className="col-md-8 pl-5">
              <div class="tab-content" id="myTabContent">
                <div
                  class="tab-pane fade show active"
                  id="home"
                  role="tabpanel"
                  aria-labelledby="home-tab"
                  
                >
                   <div className="row">
                    <div className="col-md-6">
                      <label>User ID</label>
                    </div>
                    <div className="col-md-6">
                      <p>{userData._id}</p>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <label>Name</label>
                    </div>
                    <div className="col-md-6">
                      <p>{userData.name}</p>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <label>Email</label>
                    </div>
                    <div className="col-md-6">
                      <p>{userData.email}</p>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <label>Phone</label>
                    </div>
                    <div className="col-md-6">
                      <p>{userData.phone}</p>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <label>Profession</label>
                    </div>
                    <div className="col-md-6">
                      <p>{userData.work}</p>
                    </div>
                  </div>
                 
                </div>


                
                <div class="tab-pane fade  tab-content" id="profile" role="tabpanel">
                 
                <div className="row">
                <div className="col-md-6">
                      <p>Total Project</p>
                    </div>
                    <div className="col-md-6">
                      <p>230</p>
                    </div>
                    <div className="col-md-6">
                      <p>English Level</p>
                    </div>
                    <div className="col-md-6">
                      <p>Epert</p>
                    </div>
                    <div className="col-md-6">
                      <p>Availability</p>
                    </div>
                    <div className="col-md-6">
                      <p>6 months</p>
                    </div>
                    <div className="col-md-6">
                      <p>Experience</p>
                    </div>
                    <div className="col-md-6">
                      <p>Epert</p>
                    </div>
                    <div className="col-md-6">
                      <p>Hourly Rate</p>
                    </div>
                    <div className="col-md-6">
                      <p>10$/hr</p>
                    </div>

                   </div> 
                </div>
              </div>
            </div>
          </div>








          <div className="col-md-2">
            <input type="submit" value="Edit Profile" />
          </div>
        </div>
      </form>
    </div>
  );
};

export default About;
