import React from 'react'
import "../cssfiles/Login.css";
import {Typography ,Button} from '@mui/material'
import {NavLink} from 'react-router-dom'
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';

const Errorpage = () => {
  return (
    <div style={{textAlign: "center",paddingTop:"15%"}}>
    <Typography variant='h1' sx={{color:"white"}}>
     Error 404 
    <Typography variant='h5'sx={{ color: '#e8e8e8' }}>WE ARE SORRY , PAGE IS NOT FOUND!</Typography>
    <Typography variant='h6'>The 404 code means that a server could not find a client-requested webpage.</Typography>
    </Typography>
    <KeyboardBackspaceIcon/>
    <Button><NavLink to="/"> GO BACK TO HOME PAGE</NavLink></Button>
    
   </div>
  )
}

export default Errorpage
