import React, { useEffect, useState } from "react";
import "../cssfiles/Login.css";
import { Typography } from "@mui/material";
import "bootstrap/dist/css/bootstrap.min.css";

const Contact = () => {
  const [userData, setUserData] = useState({
    name: "",
    email: "",
    phone: "",
    message: "",
  });
  const userContact = async () => {
    try {
      const res = await fetch("/gatedata", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      });
      const data = await res.json();
      setUserData({
        ...userData,
        name: data.name,
        email: data.email,
        phone: data.phone,
      });
      setUserData(data);
      console.log(data);

      if (!res.status === 200) {
        const err = new Error(res.error);
        throw err;
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    userContact();
  }, []);

  const handalChangeValue = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setUserData({ ...userData, [name]: value });
  };

  const ContactF = async (e) => {
    e.preventDefault();
    const { name, email, phone, message } = userData;
    const res = await fetch("/contact", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({name, email, phone, message}),
    });
    const data = await res.json();
    if (!data) {
      alert("message is not send");
    } else {
      alert("message is send");
      setUserData({ ...userData, message: "" });
    }
  };

  return (
    <>
      <div class="container cardd">
        <div className="row">
          <div className="col-12 col-lg-4 col-md-4 col-sm-4">
            <div className="card">
              <div className="card-body">
                <div className="card-title">
                  <span>
                    <i class="fa-duotone fa-mobile"></i>
                    <Typography></Typography>
                    {"Phone"}
                  </span>

                  <div className="card-text">
                    <Typography>{userData.phone}</Typography>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-lg-4 col-md-4 col-sm-4">
            <div className="card">
              <div className="card-body">
                <div className="card-title">
                  <span>
                    <i class="fa-duotone fa-envelope"></i>
                    <Typography>Email</Typography>
                  </span>

                  <div className="card-text">
                    <Typography>@{userData.email}</Typography>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-lg-4 col-md-4 col-sm-4">
            <div className="card">
              <div className="card-body">
                <div className="card-title">
                  <span>
                    {" "}
                    <i class="fa-duotone fa-location-dot"></i>
                    <Typography></Typography>
                    {"Address"}
                  </span>

                  <div className="card-text">
                    <Typography>Palasia Indore</Typography>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* <div className="card" style={{width: "18rem"}}> */}

      <div class="mt-5" style={{ display: "flex", justifyContent: "center" }}>
        <form
          method="POST"
          className="card shadow-lg p-3 mb-5 bg-white rounded d-flex"
          style={{ alignItems: "center" }}
        >
          <div>
            <Typography variant="h5">Get in Touch</Typography>
          </div>
          <div class="form-group card-body">
            <div className="row">
              <div className="col">
                <input
                  type="Name"
                  class="form-control"
                  id="exampleFormControlInput1"
                  name="name"
                  value={userData.name}
                  onChange={handalChangeValue}
                  placeholder="Name"
                />
              </div>
              <div className="col">
                <input
                  type="email"
                  class="form-control"
                  id="exampleFormControlInput1"
                  name="email"
                  value={userData.email}
                  onChange={handalChangeValue}
                  placeholder="name@example.com"
                />
              </div>
              <div className="col">
                <input
                  type="Phone"
                  class="form-control"
                  id="exampleFormControlInput1"
                  name="phone"
                  value={userData.phone}
                  onChange={handalChangeValue}
                  placeholder="Phone Number"
                />
              </div>

              <div class="form-group">
                <label for="exampleFormControlTextarea1">
                  Example textarea
                </label>
                <textarea
                  name="message"
                  class="form-control"
                  id="exampleFormControlTextarea1"
                  rows="3"
                ></textarea>
              </div>
              <div className="mt-3 input-box">
                <input type="submit" onClick={ContactF} value="Send Message" />
              </div>
            </div>
          </div>
        </form>
      </div>
    </>
  );
};

export default Contact;
